/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import Vue from 'vue';
import FayePlugin from '~/core/client/faye';

Vue.use(FayePlugin);

export default ({store}) => {
  Vue.prototype.$faye.connectInit();
}
