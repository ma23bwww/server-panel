/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

require('../_constants');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const Queue = require('../dist/mqtt').default;
const Backup = require('../dist/libs/backup');
const {getKey} = require('../dist/libs/os');
const Fs = require('../dist/libs/Fs').default;

const queue = new Queue();

queue.consume(Queue.EVENTS.BACKUP, async (msg) => {
  const src = path.resolve(msg.root, msg.dir);
  let dest = null;

  await getKey('sys').then(async (val) => {
    if (val && val.hasOwnProperty('backupDir') && String(val.backupDir).length >= 2) {
      await Fs.emptyDir(val.backupDir);
      dest = path.resolve(val.backupDir, (new Backup.nameItem(msg.dir, msg)).generate());
    } else {
      // todo запись в БД об невалидном пути
    }
  });

  if (dest) {
    (new Backup.Files.Pack(src, dest)).on('done', () => {
      // todo пишем в БД историю
    });
  }
});

queue.consume(Queue.EVENTS.RESTORE_BACKUP, async (msg) => {
  let src = null;
  let dest = null;

  await getKey('sys').then(async (val) => {
    if (val && val.hasOwnProperty('backupDir')) {
      src = path.resolve(val.backupDir, msg.archive);
    }
  });

  await getKey('OS').then(async (val) => {
    if (_.isObject(val) && val.hasOwnProperty('www') && String(val.www).length >= 2) {
      dest = path.resolve(val.www, msg.name);
    } else {
      // todo запись в БД об невалидном пути
    }
  });

  if (dest && !fs.existsSync(dest)) {
    await Fs.emptyDir(dest);
  } else {
    // todo запись в БД об невалидном пути
  }

  if (src && fs.existsSync(src) && fs.existsSync(dest)) {
    (new Backup.Files.Unpack(src, dest)).on('done', () => {
      // todo пишем в БД историю
    });
  }
});
