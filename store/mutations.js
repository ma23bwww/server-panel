/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

let Cookie = process.browser ? require('~/plugins/libs').Cookie : null;

const mutations = {
  SET_LANG(state, locale) {
    let dir = [];

    if (state.locales.includes(locale)) {
      state.locale = locale;
      state.dir = dir.includes(locale) ? 'rtl' : 'ltr';
      if (process.browser) {
        Cookie.set('locale', locale);
      }
    }
  },
  SET_LAUNCH(state, val) {
    if (state.isLaunch === val) {
      return;
    }
    state.requireRestartOS = false;
    state.isLaunch = val;
  },
  SET_ENV(state, obj) {
    state.ENV = obj;
  },
  RESTART_OS(state, val) {
    state.requireRestartOS = false;
    if (state.isLaunch && val) {
      state.requireRestartOS = val;
    }
  }
};

export default mutations;
