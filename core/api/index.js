/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

import API from './Api';
import Backups from './Backups';
import Domains from './Domains';
import Email from './Email';
import Ftp from './Ftp';
import Setting from './Setting';

export default {
  install(Vue, options) {
    if (!Vue.prototype.hasOwnProperty('$API')) {
      Vue.prototype.$API = new API();
      Vue.prototype.$API.Backups = new Backups();
      Vue.prototype.$API.Domains = new Domains();
      Vue.prototype.$API.Email = new Email();
      Vue.prototype.$API.Ftp = new Ftp();
      Vue.prototype.$API.Setting = new Setting();
    }
  }
};
