/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import axios from '~/plugins/axios';
import Api from './Api';

export default class Setting extends Api {
  constructor() {
    super();
    this.url = super.fullUrl(`/setting`);
  }

  getData() {
    return axios.get(`${this.url}/getData`, super.defaultOpts)
      .then(super.response);
  }

  save(data) {
    return axios.post(`${this.url}/save`, data, super.defaultOpts)
      .then(super.response);
  }
}
