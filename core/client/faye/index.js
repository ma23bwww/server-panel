/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

'use strict';

import Faye from './client';

export default {
  install(Vue, options) {
    if (!Vue.prototype.hasOwnProperty('$faye')) {
      Vue.prototype.$faye = new Faye();
    }
  }
}
