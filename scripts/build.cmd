@echo off

SET ROOT_DIR=%~dp0..
SET OUT="%~dp0\..\dist"
SET PRESENT="--presets=env"
SET PLUGINS="--plugins=transform-runtime"
SET PM2_PATH=%USERPROFILE%\AppData\Roaming\npm\pm2

IF NOT EXIST %OUT% (
    md %OUT%
)

IF NOT EXIST %PM2_PATH% (
    echo PM2 not installed, installing ...
    npm install -g pm2
)

call npm install
call babel %ROOT_DIR%\server\db --out-dir %OUT%\db %PRESENT% %PLUGINS%
call babel %ROOT_DIR%\server\mqtt --out-dir %OUT%\mqtt %PRESENT% %PLUGINS%
call babel %ROOT_DIR%\server\libs --out-dir %OUT%\libs %PRESENT% %PLUGINS%
call babel %ROOT_DIR%\server\utils --out-dir %OUT%\utils %PRESENT% %PLUGINS%

exit