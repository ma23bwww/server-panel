require('./_constants');
const {resolve} = require('path');

const CWD = resolve(__dirname);

process.env = Object.assign(process.env, {
  NODE_ENV: 'production'
});

module.exports = {
  apps: [
    {
      name: "OS Web Panel",
      cwd: CWD,
      script: "build/main.js",
      // script: "npm",
      // args: ["run", "start", "--scripts-prepend-node-path"],

      log_date_format: "YYYY-MM-DD HH:mm:ss",
      error_file: resolve(LOGS_DIR, 'osp_error.log'),
      out_file: resolve(LOGS_DIR, 'osp_out.log'),
      watch: false,
      ignore_watch: [
        "node_modules"
      ],

      exec_interpreter: "node",
      exec_mode: "fork"
    }
    ,
    {
      name: "OS Service | Backup",
      cwd: CWD,
      script: "workers/backup.service.js",

      log_date_format: "YYYY-MM-DD HH:mm:ss",
      error_file: resolve(LOGS_DIR, 'backup_error.log'),
      out_file: resolve(LOGS_DIR, 'backup_out.log'),
      watch: false,
      ignore_watch: [
        "node_modules"
      ],

      exec_interpreter: "node",
      exec_mode: "fork"
    }
  ]
};