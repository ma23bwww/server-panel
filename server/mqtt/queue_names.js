/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

module.exports = {
  BACKUP: 'backup',
  RESTORE_BACKUP: 'restore_backup'
};
