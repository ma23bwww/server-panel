/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 https://github.com/louischatriot/nedb
 */

import mqtt from 'mqtt';
import {EventEmitter} from 'events';
import {isJson} from '../utils';
import Server from './server';

const EVENTS = require('./queue_names');

const noop = () => {
};

export default class Queue extends EventEmitter {
  constructor() {
    super();

    const port = process.env.QUEUE_PORT || 1883
    this._qn = 0;

    this._client = mqtt.connect(`mqtt://localhost:${port}`, {
      connectTimeout: 350,
      reconnectPeriod: 300,
      protocolId: 'MQIsdp',
      clientId: this._hash
    });

    this._client.on('error', this.Error);

    this._client.on('connect', () => {
      this.emit('connect')
    });

    this._client.on('disconnect', () => {
      this.emit('disconnect');
    });
  }

  /**
   * подключаем константы с названиями очередей
   * @constructor
   */
  static get EVENTS() {
    return EVENTS;
  }

  Error(e) {
    console.error('[MQTT]', e);
    return e;
  }

  /**
   * Упаковка данных
   * @param data
   * @private
   */
  _encode(data = {}) {
    return new Buffer(JSON.stringify(data));
  }

  get _hash() {
    return Math.random().toString(36).substr(2).toString('hex');
  }

  /**
   * Распаковка данных
   * @param data
   * @returns {*}
   * @private
   */
  _decode(data) {
    if (isJson(data)) {
      return JSON.parse(data);
    }
    return data;
  }

  close() {
    try {
      this._client.end();
    } catch (e) {
    }
  }

  /**
   * Добавляем данные в очередь
   * @param queueName
   * @param data
   * @returns {Promise}
   */
  addToQueue(queueName, data = {}) {
    return new Promise((resolve, reject) => {
      const onError = e => {
        this.Error(e);

        // При ошибке, пробуем записать еще раз
        setTimeout(() => this.addToQueue(queueName, data).catch(reject), 2e3);
      };

      // ++this._qn;

      this._client.publish(queueName, this._encode(data), {qos: 0}, () => {
        resolve();
      });
    });
  }

  /**
   * Подписка на очередь
   * @param queueName
   * @param cb
   */
  consume(queueName, cb = noop) {
    this._client.subscribe(queueName);
    this._client.on('message', (topic, message, packet) => {
      if (topic === queueName) {
        cb(this._decode(message));
      }
    });
  }

  unsubscribe(queueName) {
    this._client.unsubscribe(queueName);
  }
}

export {
  Server
};
