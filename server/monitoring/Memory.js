/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import {EventEmitter} from 'events';
import {freemem, totalmem} from 'os';

export default class NativeMemory extends EventEmitter {
  constructor() {
    super();

    this._pause = 1e3;

    this._getData();
  }

  _getData() {
    let freeMem = freemem();
    let totalMem = totalmem();
    let freePercent = Number((freeMem / totalMem * 100).toFixed(2));
    let usePercent = 100 - freePercent;

    super.emit('data', {
      freeMem,
      totalMem,
      usePercent,
      freePercent
    });

    setTimeout(() => this._getData(), this._pause);
  }

  setPause(val) {
    this._pause = val;
  }
}
