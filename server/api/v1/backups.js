import {Router} from 'express';
import path from 'path';
import Queue from '../../mqtt';
import {getKey} from '../../libs/os';
import Fs from '../../libs/Fs';

const {nameItem, GenerateList} = require('../../libs/backup');

const router = Router();
const MU = '/backups';
const queue = new Queue();

router.get(`${MU}/getList`, (req, res) => {

  const onError = e => {
    res.sendStatus(500);
  };

  getKey('sys').then(async (val) => {
    if (val && val.hasOwnProperty('backupDir')) {
      let dir = path.resolve(val.backupDir);

      return Fs.dirList(dir)
        .then(list => {
          const gl = new GenerateList();
          list.forEach(item => {
            if (String(item).includes('.tar.gz')) {
              gl.add(new nameItem(item).parse());
            }
          });
          res.send(gl.get());
        })
        .catch(onError);
    }
    res.sendStatus(500);
  }).catch(onError);
});

router.post(`${MU}/create`, (req, res) => {
  queue.addToQueue(Queue.EVENTS.BACKUP, req.body)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.post(`${MU}/restore`, (req, res) => {
  queue.addToQueue(Queue.EVENTS.RESTORE_BACKUP, req.body)
    .then(() => res.sendStatus(201))
    .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:name`, (req, res) => {
  getKey('sys')
    .then(async (val) => {
      if (val && val.hasOwnProperty('backupDir')) {
        let src = path.resolve(val.backupDir, req.params.name);
        return Fs.remove(src)
          .then(() => res.sendStatus(200))
          .catch(e => res.sendStatus(500));
      }
      res.sendStatus(500);
    })
    .catch(e => res.sendStatus(500));
});

export default router;
