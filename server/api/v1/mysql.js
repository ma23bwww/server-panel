import {Router} from 'express';
import Domains from '../../libs/os/Domains';
import {exec} from 'child_process';
import path from 'path';

const router = Router();
const MU = '/domains';

router.get(`${MU}/getList`, (req, res) => {
  // (new Domains()).getListWWW()
  //   .then(list => res.json(list))
  //   .catch(e => res.sendStatus(500));
});

// router.post(`${MU}/open-dir`, (req, res) => {
//   exec(`start "" "${path.resolve(req.body.root, req.body.dir)}"`, (err) => {
//     if (err) {
//       return res.sendStatus(500);
//     }
//     res.sendStatus(201);
//   })
// });

router.post(`${MU}/save`, (req, res) => {
  // (new Domains()).save(req.body.name)
  //   .then(() => res.sendStatus(201))
  //   .catch(e => res.sendStatus(500));
});

router.delete(`${MU}/remove/:name`, (req, res) => {
  // (new Domains()).remove(req.params.name)
  //   .then(() => res.sendStatus(200))
  //   .catch(e => res.sendStatus(500));
});

export default router;
