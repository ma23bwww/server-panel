/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import {EventEmitter} from 'events';

const faye = require('faye');
const deflate = require('permessage-deflate');

const getChannel = (channel) => {
  return '/' + ['client', channel].join('/');
};

export default class Faye extends EventEmitter {
  constructor() {
    super();

    this.bayeux = new faye.NodeAdapter({
      mount: '/client',
      timeout: 45
    });
    this.bayeux.addWebsocketExtension(deflate);
  }

  sendClient(channel, data) {
    this.bayeux.getClient().publish(getChannel(channel), data);
  }

  attach(server) {
    this.bayeux.attach(server);
  }
}