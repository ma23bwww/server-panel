/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Класс для работы с файлом init.ini
 */

import ini from 'ini';
import path from 'path';
import OpenServer from "./index";
import Fs from '../Fs';

export default class Init extends OpenServer {
  constructor() {
    super();

    this._file = path.resolve(this.userdataPath, 'init.ini');
  }

  getData() {
    return new Promise(async (resolve, reject) => {
      try {
        const init = ini.parse(Fs.readFileSync(this._file));
        resolve(init);
      } catch (e) {
        reject(e);
      }
    });
  }

  saveData(json = {}) {
    return this.getData().then(init => {
      init.main = Object.assign(init.main, json);

      return Fs.writeFile(this._file, ini.stringify(init));
    });
  }
}