/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 Класс для работы с файлом init.ini
 */

import ini from 'ini';
import path from 'path';
import OpenServer from "./index";
import Fs from '../Fs';

export default class Profile extends OpenServer {
  constructor(name) {
    super();

    this._main = path.resolve(this.profilesPath, `${name}.ini`);

    ['aliases', 'autostart', 'cron', 'domains'].forEach(file => {
      this[`_${file}`] = path.resolve(this.profilesPath, `${name}_${file}.txt`);
    });
  }

  getData() {
    return new Promise(async (resolve, reject) => {
      try {
        const init = ini.parse(Fs.readFileSync(this._main));
        resolve(init);
      } catch (e) {
        reject(e);
      }
    });
  }

  saveData(json = {}) {
    return this.getData().then(data => {
      data = Object.assign(data, json);
      return Fs.writeFile(this._main, ini.stringify(data));
    });
  }
}