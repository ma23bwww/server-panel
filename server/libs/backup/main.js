/**
 Copyright © Oleg Bogdanov
 Developer: Oleg Bogdanov
 Contacts: https://github.com/wormen
 ---------------------------------------------
 */

import {EventEmitter} from 'events';

export default class Backup extends EventEmitter {
  constructor() {
    super();
    this.on('error', this.Error);
  }

  Error(e) {
    console.error(e);
    return e;
  }
}
